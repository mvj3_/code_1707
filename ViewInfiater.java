ViewHolder holder;
			// 观察convertView随ListView滚动情况
			Log.v("MyListViewBase", "getView " + position + " " + convertView);
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.vlist, null);
				holder = new ViewHolder();
				/** 得到各个控件的对象 */
				holder.title = (TextView) convertView.findViewById(R.id.title);
				holder.xingji = (TextView) convertView
						.findViewById(R.id.xingji);
				convertView.setTag(holder);// 绑定ViewHolder对象
			} else {
				holder = (ViewHolder) convertView.getTag();// 取出ViewHolder对象
			}